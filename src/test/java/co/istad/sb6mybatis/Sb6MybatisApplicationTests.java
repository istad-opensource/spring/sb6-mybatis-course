package co.istad.sb6mybatis;

import co.istad.sb6mybatis.model.Grade;
import co.istad.sb6mybatis.model.Student;
import co.istad.sb6mybatis.model.User;
import co.istad.sb6mybatis.repository.StudentRepository;
import co.istad.sb6mybatis.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
class Sb6MybatisApplicationTests {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    void testSelectUser() {
        List<User> users = userRepository.select("dara",
                true,
                true);
        users.forEach(System.out::println);
    }

    @Test
    void testInsertUser() {
        User user = User.builder()
                .username("chandara")
                .password("123")
                .createdAt(LocalDateTime.now())
                .status(true)
                .build();
        userRepository.insert(user);
        System.out.println(user);
        userRepository.createUserRole(user.getId(), 1);
        userRepository.createUserRole(user.getId(), 2);
    }

    @Test
    void testInsertStudent() {

        Student student = Student.builder()
                .familyName("Lionel")
                .givenName("Messi")
                .email("leomessi@footballer.com")
                .status(true)
                .grade(Grade.builder().id(1).build())
                .build();

        studentRepository.insert(student);

    }

    @Test
    void testSelectStudentById() {
        System.out.println(studentRepository.selectById(3));
    }

}
