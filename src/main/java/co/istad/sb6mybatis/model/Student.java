package co.istad.sb6mybatis.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Student {

    private Integer id;
    private String familyName;
    private String givenName;
    private String email;
    private Boolean status;
    private Grade grade;

}
