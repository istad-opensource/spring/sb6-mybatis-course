package co.istad.sb6mybatis.model;


import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
@ToString
public class User {

    private Integer id;
    private String username;
    private String password;
    private LocalDateTime createdAt;
    private Boolean status;
    private List<Role> roles;

}
