package co.istad.sb6mybatis.model;

import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
@ToString
public class Role {

    private Integer id;
    private String name;

}
