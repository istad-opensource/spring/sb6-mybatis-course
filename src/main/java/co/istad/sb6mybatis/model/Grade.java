package co.istad.sb6mybatis.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Grade {

    private Integer id;
    private String name;
    private String level;
    private Boolean status;

}
