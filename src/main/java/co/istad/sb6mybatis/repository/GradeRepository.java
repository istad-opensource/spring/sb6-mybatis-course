package co.istad.sb6mybatis.repository;

import co.istad.sb6mybatis.model.Grade;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface GradeRepository {

    @Select("SELECT * FROM grades WHERE id = #{id}")
    Grade selectById(@Param("id") Integer id);

}
