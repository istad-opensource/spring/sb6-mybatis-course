package co.istad.sb6mybatis.repository;

import co.istad.sb6mybatis.model.Role;
import co.istad.sb6mybatis.model.User;
import co.istad.sb6mybatis.repository.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.builder.annotation.ProviderMethodResolver;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserRepository {

    @SelectProvider(UserProvider.class)
    @Results(id = "userResultMap", value = {
            @Result(column = "id", property = "id"),
            @Result(column = "created_at", property = "createdAt"),
            @Result(column = "id", property = "roles",
                many = @Many(select = "selectUserRoles"))
    })
    List<User> select(@Param("keyword") String keyword,
                      @Param("isAdmin") Boolean isAdmin,
                      @Param("isActive") Boolean isActive);

    @SelectProvider(UserProvider.class)
    List<Role> selectUserRoles(@Param("userId") Integer userId);

    @InsertProvider(UserProvider.class)
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    void insert(@Param("u") User user);

    @InsertProvider(UserProvider.class)
    void createUserRole(@Param("userId") Integer userId,
                        @Param("roleId") Integer roleId);

}
