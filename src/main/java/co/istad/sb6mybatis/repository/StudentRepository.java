package co.istad.sb6mybatis.repository;

import co.istad.sb6mybatis.model.Grade;
import co.istad.sb6mybatis.model.Student;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface StudentRepository {

    @Insert("""
                INSERT INTO students (family_name, given_name, email, status, grade_id)
                VALUES (#{s.familyName}, #{s.givenName}, #{s.email}, #{s.status}, #{s.grade.id})
            """)
    void insert(@Param("s") Student student);

    @Update("""
                UPDATE students
                SET family_name = #{s.familyName}, given_name = #{s.givenName}
                WHERE id = #{s.id}
            """)
    void updateBasicInfoById(@Param("s") Student student);

    @Delete("DELETE FROM students WHERE id = #{id}")
    void deleteById(@Param("id") Integer id);

    @Select("SELECT * FROM students WHERE id = #{id}")
    @Results(id = "studentResultMap", value = {
            @Result(column = "family_name", property = "familyName"),
            @Result(column = "given_name", property = "givenName"),
            @Result(column = "grade_id", property = "grade",
                    one = @One(select = "co.istad.sb6mybatis.repository.GradeRepository.selectById"))
    })
    Student selectById(@Param("id") Integer id);

    @Select("SELECT * FROM students")
    @ResultMap("studentResultMap")
    List<Student> select();

}
