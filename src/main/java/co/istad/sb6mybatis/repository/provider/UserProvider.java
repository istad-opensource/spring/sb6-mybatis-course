package co.istad.sb6mybatis.repository.provider;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.builder.annotation.ProviderMethodResolver;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider implements ProviderMethodResolver {

    private final String TB_USERS = "users";
    private final String TB_USERS_ROLES = "users_roles";

    public String select(@Param("keyword") String keyword,
                         @Param("isAdmin") Boolean isAdmin,
                         @Param("isActive") Boolean isActive) {
        return new SQL() {{
            SELECT("id", "username");

            if (isAdmin) {
                SELECT("password");
            }

            SELECT("created_at", "status");

            FROM(TB_USERS);

            if (!keyword.isBlank()) {
                WHERE("username ILIKE '%' || #{keyword} || '%'");
            }

            if (isActive)
                WHERE("status = TRUE");
            else
                WHERE("status = FALSE");

        }}.toString();
    }

    public String selectUserRoles() {
        return new SQL() {{
            SELECT("r.id, r.name");
            FROM("roles AS r");
            INNER_JOIN("users_roles AS ur ON ur.role_id = r.id");
            WHERE("ur.user_id = #{userId}");
        }}.toString();
    }

    public String insert() {
        return new SQL() {{
            INSERT_INTO(TB_USERS);
            VALUES("username", "#{u.username}");
            VALUES("password", "#{u.password}");
            VALUES("created_at", "#{u.createdAt}");
            VALUES("status", "#{u.status}");
        }}.toString();
    }

    public String createUserRole() {
        return new SQL() {{
            INSERT_INTO(TB_USERS_ROLES);
            VALUES("user_id", "#{userId}");
            VALUES("role_id", "#{roleId}");
        }}.toString();
    }

}
