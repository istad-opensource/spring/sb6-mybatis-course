package co.istad.sb6mybatis.controller;

import co.istad.sb6mybatis.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    public String viewStudent(ModelMap modelMap) {

        modelMap.addAttribute("students",
                studentService.select());

        return "student";
    }

}
