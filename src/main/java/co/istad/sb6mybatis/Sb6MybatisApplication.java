package co.istad.sb6mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sb6MybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sb6MybatisApplication.class, args);
    }

}
