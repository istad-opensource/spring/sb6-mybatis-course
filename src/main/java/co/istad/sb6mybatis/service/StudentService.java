package co.istad.sb6mybatis.service;

import co.istad.sb6mybatis.model.Student;

import java.util.List;

public interface StudentService {

    List<Student> select();

}
