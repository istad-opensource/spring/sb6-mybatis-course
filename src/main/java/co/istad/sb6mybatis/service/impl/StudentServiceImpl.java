package co.istad.sb6mybatis.service.impl;

import co.istad.sb6mybatis.model.Student;
import co.istad.sb6mybatis.repository.StudentRepository;
import co.istad.sb6mybatis.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public List<Student> select() {
        return studentRepository.select();
    }
}
